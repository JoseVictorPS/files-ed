#ifndef FILES_H
#define FILES_H

#include <stdio.h>
#include <stdlib.h>

FILE * open_f(const char *name, const char*mode);
FILE * build_TXT(char*name);
void printf_f(FILE*fp, char*name);
int word_count(FILE*fp, char*name);
int line_count(FILE*fp, char*name);
FILE * build_INT(char*name);

FILE * open_f(const char *name, const char*mode) {
    FILE*fp = fopen(name, mode);
    if(!fp) {printf("Erro ao abrir arquivo!\n"); exit(1);}
    return fp;
}

FILE * build_TXT(char*name) {
    int op=1;
    FILE*fp=open_f(name, "w");
    while(op) {
        printf("Digite uma palavra para o arquivo: ");
        char p[20];
        scanf("%s", p);
        fputs(p, fp);
        fputs(" ", fp);
        printf("Deseja operar novamente? ");
        scanf("%d", &op);
    }
    fclose(fp);
    printf_f(fp, name);
    return fp;
}

void printf_f(FILE*fp, char*name) {
    fp=open_f(name, "r");
    int ch;
    while((ch=fgetc(fp)) != EOF) printf("%c", ch);
    printf("\n");
    fclose(fp);
}

int word_count(FILE*fp, char*name) {
    fp=open_f(name, "r");
    int n=0;
    char str[80];
    while(fscanf(fp, "%s", str)==1) n++;
    fclose(fp);
    return n;
}

int line_count(FILE*fp, char*name) {
    char c;
    int linha=0;
    fp=fopen(name, "r");
    while(fscanf(fp, "%c", &c)==1) if(c=='\n') linha++;
    return linha;
}

FILE * build_INT(char*name) {
    int op=1;
    FILE*fp=open_f(name, "w");
    int c=1;
    while(op) {
        printf("Digite um inteiro para o arquivo: ");
        int i;
        scanf("%d", &i);
        fprintf(fp, "%d", i);
        if(c%5==0) fputs("\n",fp);
        c++;
        printf("Deseja operar novamente? ");
        scanf("%d", &op);
    }
    fclose(fp);
    printf_f(fp, name);
    return fp;
}

#endif //FILES_H