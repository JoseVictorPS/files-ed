#ifndef RESPOSTAS_H
#define RESPOSTAS_H

#include "files.h"
#include <string.h>

void remove_repeat(FILE*fp);
static void insert_vet(char*str, char vet[][80], int*i);
static void insert_file(FILE*fp, char vet[][80], int i);
FILE * build_ALU(char*name);
void mean(FILE*in, FILE*out);
static int cmpal(const void *alu, const void*no);
int read_int();
void count_n(char*name, int n);
FILE * build_BIN(char*nome);
void print_bin(char*nome);
void bubble_bin(char*nome);

void remove_repeat(FILE*fp) {
    int n = word_count(fp, "teste.txt");
    char vet[n][80];
    fp=open_f("teste.txt", "r");
    char str[80];
    int i=0;
    while(fscanf(fp, "%s", str)==1)insert_vet(str, vet, &i);
    fclose(fp);

    insert_file(fp, vet, i);
    printf_f(fp, "teste.txt");
}

static void insert_vet(char*str, char vet[][80], int *i) {
    int j = (*i);
    if(!j){strcpy(vet[0], str); (*i)++;}
    else {
        int repeat=0;
        for(int k=0; k<j; k++)if(!strcmp(str, vet[k]))repeat=1;
        if(!repeat){strcpy(vet[j], str); (*i)++;}
    }
}

static void insert_file(FILE*fp, char vet[][80], int i) {
    fp=open_f("teste.txt", "w");
    for(int k=0; k<i; k++){ fputs(vet[k], fp); fputs(" ", fp); }
    fclose(fp);
}

typedef struct aluno {
    char nome[3];
    float media;
}Aluno;

FILE * build_ALU(char*name) {
    int op=1;
    FILE*fp=open_f(name, "w");
    while(op) {
        printf("Digite um nome e as notas da prova: ");
        char p[3];
        float p1, p2;
        scanf("%s%f%f", p, &p1, &p2);
        fprintf(fp, "%s %f %f", p, p1, p2);
        fputs("\n", fp);
        printf("Deseja operar novamente? ");
        scanf("%d", &op);
    }
    fclose(fp);
    printf_f(fp, name);
    return fp;
}

void mean(FILE*in, FILE*out) {
    in=open_f("entrada.txt", "r");
    int n = line_count(in, "entrada.txt");
    Aluno vet[n];
    float p1, p2;
    int count=0;
    while(fscanf(in, "%s %f %f", vet[count].nome, &p1, &p2)==3) {
        vet[count].media = (p1+p2)/2;
        count++;
    }
    fclose(in);
    qsort(vet, n, sizeof(Aluno), cmpal);
    out = open_f("saida.txt", "w");
    for(int i=0; i<n; i++) {
        fprintf(out, "%s %f", vet[i].nome, vet[i].media);
        fputs("\n", out);
    }
    fclose(out);
    printf_f(out, "saida.txt");
}

static int cmpal(const void *alu, const void*no) {
    Aluno*a1 = (Aluno*)alu, *b1 = (Aluno*)no;
    Aluno a = (*a1), b = (*b1);
    if(a.media > b.media) return 1;
    if (a.media < b.media) return -1;
    return 0;
}

int read_int() {
    printf("Digite um inteiro: ");
    int n;
    scanf("%d", &n);
    return n;
}

void count_n(char*name, int n) {
    char c;
    int li=0, total=0, nl=1, tc;
    FILE*fp=fopen(name, "r");
    while(fscanf(fp, "%c", &c)==1) {
        tc = (int)c-48;
        if(tc==n) {li++; total++;}
        if(c=='\n'){printf("%d ocorrencias na linha %d\n",li,nl);li=0;nl++;}
    }
    printf("%d ocorrencias totais!\n", total);
    fclose(fp);
}

FILE * build_BIN(char*nome) {
    int op=1;
    FILE*fp=open_f(nome, "wb");
    while(op) {
        printf("Digite um inteiro para o arquivo: ");
        int i;
        scanf("%d", &i);
        fwrite(&i, sizeof(int), 1, fp);
        printf("Deseja operar novamente? ");
        scanf("%d", &op);
    }
    fclose(fp);
    print_bin(nome);
    return fp;
}

void print_bin(char*nome) {
    FILE*fp=open_f(nome, "rb");
    int p;
    while(fread(&p, sizeof(int), 1, fp)==1)printf("%d", p);
    printf("\n");
    fclose(fp);
}

void bubble_bin(char*nome) {
    FILE*fp = open_f(nome, "rb");
}

#endif //RESPOSTAS_H